# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${COREBASE}/meta/files/common-licenses/Apache-2.0;md5=89aea4e17d99a7cacdbeed46a0096b10"

DEPENDS = "mixxx"

SRC_URI = "file://oniroglove.midi.xml"

do_install:append () {
  install -d "${D}${datadir}/mixxx/controllers/"
  install -m 0755 "${WORKDIR}/oniroglove.midi.xml" "${D}${datadir}/mixxx/controllers/"
}

FILES:${PN} = "${datadir}/mixxx/controllers/oniroglove.midi.xml"

