# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

HOMEPAGE = "https://docs.oniroproject.org/"
SUMMARY = "DJ Station blueprint image"
LICENSE = "Apache-2.0"

require recipes-core/images/oniro-image-base.bb

REQUIRED_DISTRO_FEATURES = "wayland"



IMAGE_INSTALL:append = "\
    packagegroup-dj-qt5 \
    faad2 \
    mixxx \
    mixxxservice \
    djgloveprefs \
    mixxx-usb-uaccess \
    qtwayland \
    weston \
    weston-init \
"

IMAGE_INSTALL:append = " pulseaudio-server libpulsecore sbc \
pulseaudio-module-bluetooth-discover pulseaudio-module-bluez5-device \
pulseaudio-module-bluez5-discover pulseaudio-module-loopback \
pulseaudio-module-rtp-send pulseaudio-module-rtp-recv \
pulseaudio-module-bluetooth-policy pulseaudio-misc dbus gconf \
pulseaudio-module-alsa-card pulseaudio-module-alsa-source \
pulseaudio-module-alsa-sink pulseaudio-module-cli libasound \
libpulse libsndfile1 alsa-utils alsa-utils-speakertest pulseaudio-module-dbus-protocol"

# Nice clever way to populate our mixxx prefs and music dir
WIC_ROOTA_PARTITION_EXTRA_ARGS = "--exclude-path=run/mount/appdata"
WIC_ROOTB_PARTITION_EXTRA_ARGS = "--exclude-path=run/mount/appdata"
WIC_APPDATA_PARTITION_EXTRA_ARGS = "--source rootfs --rootfs-dir=${IMAGE_ROOTFS}/run/mount/appdata"
${APPDATA_PARTITION_SIZE} = "4G"
ROOTFS_POSTPROCESS_COMMAND:append = " add_mixxx_dir;"

add_mixxx_dir () {
  mkdir -p ${IMAGE_ROOTFS}/run/mount/appdata/music/.mixxx
  mkdir -p ${IMAGE_ROOTFS}/run/mount/appdata/music/Music
  cp ${IMAGE_ROOTFS}/usr/share/mixxx/tmp/* ${IMAGE_ROOTFS}/run/mount/appdata/music/
  chown -R 1000:1000 ${IMAGE_ROOTFS}/run/mount/appdata/music
}

