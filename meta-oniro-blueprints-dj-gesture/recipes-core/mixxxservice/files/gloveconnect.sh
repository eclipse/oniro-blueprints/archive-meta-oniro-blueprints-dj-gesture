# SPDX-FileCopyrightText: 2021 Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

#!/usr/bin/expect -f

set prompt "#"
set address [lindex $argv 0]

spawn bluetoothctl
expect -re $prompt
send "remove $address\r"
sleep 1
expect -re $prompt
send "scan on\r"
send_user "\nSleeping\r"
sleep 5
send_user "\nDone sleeping\r"
send "scan off\r"
send "connect $address\r"
sleep 5
send "quit\r"
expect eof
