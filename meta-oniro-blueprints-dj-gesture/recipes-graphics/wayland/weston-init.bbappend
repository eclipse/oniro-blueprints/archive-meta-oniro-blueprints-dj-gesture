# SPDX-FileCopyrightText: 2021 Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

FILESEXTRAPATHS:prepend := "${THISDIR}/files:"

USERADD_PARAM:${PN} = "--home /run/mount/appdata/music --shell /bin/sh --user-group -G video,input weston"
WESTON_INI_BACKGROUND_COLOR = "0x0f0026"
