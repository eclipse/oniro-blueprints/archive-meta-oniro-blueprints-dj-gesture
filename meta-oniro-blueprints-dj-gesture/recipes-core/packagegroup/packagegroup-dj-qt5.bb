# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

SUMMARY = "Packagegroup which provides needed QT5 libraries"

inherit packagegroup

PROVIDES = "${PACKAGES}"
PACKAGES += " \
    ${PN}-fonts \
    ${PN}-libs \
"

RRECOMMENDS_${PN} = " \
    ${PN}-fonts \
    ${PN}-libs \
"

SUMMARY_${PN}-fonts = "Some fonts useful for QT5"
RRECOMMENDS_${PN}-fonts = " \
    ttf-dejavu-common \
    ttf-dejavu-sans \
    ttf-dejavu-sans-mono \
    ttf-dejavu-serif \
"

SUMMARY_${PN}-libs = "QT5 libraries"
RRECOMMENDS_${PN}-libs = " \
    qt3d \
    qt5ledscreen \
    qtbase \
    qtbase-plugins \
    qtcharts \
    qtcoap \
    qtconnectivity \
    qtdatavis3d \
    qtdeclarative \
    qtgamepad \
    qtgraphicaleffects \
    qtimageformats \
    qtknx \
    qtlocation \
    qtlottie \
    qtmqtt \
    qtmultimedia \
    qtnetworkauth \
    qtopcua \
    qtpurchasing \
    qtquick3d \
    qtquickcontrols \
    qtquickcontrols2 \
    qtquicktimeline \
    qtremoteobjects \
    qtscript \
    qtscxml \
    qtsensors \
    qtserialbus \
    qtserialport \
    qtsvg \
    qtsystems \
    qttools \
    qttranslations \
    qtvirtualkeyboard \
    qtwayland \
    qtwebchannel \
    qtwebglplugin \
    qtwebsockets \
    qtxmlpatterns \
"
